import React from "react";
import { createStackNavigator, createSwitchNavigator, createDrawerNavigator } from 'react-navigation';
import HomePage from './src/pages/home/home';
import SignUpPage from './src/pages/signup/signup';
import LoginPage from './src/pages/login/login';
import {
  StyleSheet, ScrollView, View, Image, Text
} from "react-native";
import { DrawerItems, SafeAreaView } from 'react-navigation';

const CustomDrawerContentComponent = (props) => (
  <ScrollView>
    <SafeAreaView style={styles.container} forceInset={{ top: 'always', horizontal: 'never' }}>
      <View style={{flex: 1, alignItems: "center", padding: 10}}>
        <View style={{width: 100, height: 100, borderRadius: 50, backgroundColor: "red", overflow: "hidden"}}>
          <Image 
            style={{width: 100, height: 100}}
            source={require("./assets/profile.jpg")}
          />
        </View>
        <Text style={{padding: 10}}>charles-franca@live.com</Text>
      </View>
      <DrawerItems {...props} />
    </SafeAreaView>
  </ScrollView>
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

const homeNavigator = createDrawerNavigator({
  Home: createStackNavigator({
    screen: HomePage
  })
}, {
  contentComponent: CustomDrawerContentComponent
});

const authNavigator = createStackNavigator({
  Login: {
    screen: LoginPage
  },
  SignUp: {
    screen: SignUpPage
  }
});

export default createSwitchNavigator({
  HomeStack: homeNavigator,
  AuthStack: authNavigator
}, {
  initialRouteName: "HomeStack"
})