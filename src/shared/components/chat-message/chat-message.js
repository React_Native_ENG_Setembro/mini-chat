import React from "react";
import {
  StyleSheet,
  Text,
  View
} from "react-native";

export default class ChatMessage extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.imageConfig}>
            {/* Image */}

        </View>
        <View style={styles.messageConfig}>
            {/* Conteúdo/Mensagem */}
            <Text>{ this.props.message.message }</Text>
        </View>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "space-between",
    flexDirection: "row-reverse",
    marginTop: 30
  },
  imageConfig: {
    width: 80,
    height: 80,
    borderRadius: 40,
    backgroundColor: "red",
    marginLeft: 10,
    marginRight: 10
  } ,
  messageConfig: {
    flexGrow: 1,
    padding: 30,
    backgroundColor: "lightgreen",
    borderRadius: 20,
    marginLeft: 10,
    marginRight: 10
  }
});
