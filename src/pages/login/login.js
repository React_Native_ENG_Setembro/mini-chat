import React from "react";
import { StyleSheet, Text, View, TextInput, Button, Image, KeyboardAvoidingView } from "react-native";
import { inputStyle } from "../../shared/styles/inputStyle";
import {userService} from "../../services/user-service"; //
export default class LoginPage extends React.Component {
  static navigationOptions = {
    header: null,
    title: "Login"
  };

  state = {
    email: "",
    password: ""
  }

  login() {
    const usermodel = {
      email: this.state.email,
      password: this.state.password
    }
    userService.login(usermodel).then(user => {
      // Gravar autenticação no localstorage
      this.props.navigation.navigate("HomeStack");
    }).catch(error => {
      alert(error.message);
    });
  }

  render() {
    return (
      <KeyboardAvoidingView style={styles.container} behavior="padding" enabled>
        <View style={styles.container}>
          <Image source={require("../../../assets/chat_logo.jpg")} />
          <TextInput 
            placeholder={"E-mail..."} 
            style={inputStyle}
            onChangeText={(email) => this.setState({email}) } />
          <TextInput
            secureTextEntry={true}
            placeholder={"Senha..."}
            onChangeText={(password) => this.setState({password}) }
            style={inputStyle}

          />
          <Button
            onPress={() => {
              this.login(); // método de login
            }}
            title={"Enviar"}
          />
          <Button
            onPress={() => {
              this.props.navigation.navigate("SignUp");
            }}
            title={"Cadastre-se"}
          />
        </View>
      </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center"
  },

  input: {
    width: 300,
    borderColor: "black",
    borderWidth: 1,
    marginBottom: 10,
    padding: 10
  }
});
