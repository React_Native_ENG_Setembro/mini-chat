import React from "react";
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  Button,
  TouchableWithoutFeedback,
  ScrollView,
  FlatList
} from "react-native";
import { Ionicons } from "@expo/vector-icons";
import ChatMessage from "../../shared/components/chat-message/chat-message";
import { inputStyle } from "../../shared/styles/inputStyle";
import { userService } from "../../services/user-service";

export default class HomePage extends React.Component {
  static navigationOptions = ({ navigation }) => {
    return {
      title: "Home Page",
      headerLeft: (
        <View style={{ marginLeft: 15 }}>
          <TouchableWithoutFeedback
            onPress={() => {
              navigation.openDrawer();
            }}
          >
            <Ionicons name="md-menu" size={32} color="green" />
          </TouchableWithoutFeedback>
        </View>
      ),
      headerRight: (
        <View style={{ marginRight: 15 }}>
          <TouchableWithoutFeedback
            onPress={() => {
              navigation.navigate("AuthStack");
            }}
          >
            <Ionicons name="ios-log-out" size={32} color="green" />
          </TouchableWithoutFeedback>
        </View>
      )
    };
  };

  state = {
    message: "",
    messages: []
  }

  componentDidMount() {
    userService
      .getMessages()
      .on("value", snapshot => {
        const messages = [];
        snapshot.forEach(message => {
          messages.push(message.val());
        });
        this.setState({ messages: messages });
        console.log(this.state.messages);
      });
  }

  sendMessage() {
    userService.sendMessage(this.state.message).then(() => {
      console.log("mensagem enviada");
    })
  }

  render() {
    return (
      <View style={styles.container}>
        <ScrollView>
          <FlatList 
            data={this.state.messages}
            renderItem={({item}) => <ChatMessage message={item} />}
          />
        </ScrollView>
        <View style={{height: 80, borderTopColor: "gray", borderTopWidth: 1}}>
          <View style={styles.inputView}>
            <TextInput 
              onChangeText={(message) => this.setState({message})}
              style={inputStyle} />
            <Button onPress={() => {
              this.sendMessage();
            }} title={"Enviar"} />
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff"
  },

  inputView: {
    flex: 1,
    flexDirection: "row",
    padding: 15,
    width: "100%"
  }
});
