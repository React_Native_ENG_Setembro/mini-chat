import React from "react";
import { StyleSheet, Text, View, TextInput, Button, Image } from "react-native";
import { inputStyle } from "../../shared/styles/inputStyle";
import { userService } from "../../services/user-service";

export default class SignUpPage extends React.Component {
  state = {
    name: "",
    email: "",
    nickname: "",
    password: ""
  }

  saveUser() {
    userService.createUser({
      email: this.state.email,
      password: this.state.password
    }).then(data => {
      this.props.navigation.navigate("HomeStack");
    }).catch(error => {
      alert(error.message);
    });
  }
  
  render() {
    return (
      <View style={styles.container}>
        <Image 
          source={require("../../../assets/chat_logo.jpg")}
          />
        <TextInput 
          onChangeText={(name) => this.setState({name})} 
          placeholder={"Nome..."} style={[inputStyle]} 
          />
        <TextInput 
          onChangeText={(email) => this.setState({email})} placeholder={"E-mail..."} style={inputStyle} />
        <TextInput 
          onChangeText={(nickname) => this.setState({nickname})} placeholder={"Apelido..."} style={inputStyle} />
        <TextInput
          onChangeText={(password) => this.setState({password})}
          secureTextEntry={true}
          placeholder={"Senha..."}
          style={inputStyle}
        />
        <Button onPress={() => {
          this.saveUser();
          // this.props.navigation.navigate("HomeStack");
        }} title={"Enviar"} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center"
  }
});
