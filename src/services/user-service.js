import * as firebase from "firebase";

// Initialize Firebase
const firebaseConfig = {
  apiKey: "AIzaSyClHvGcCQceopDHyVsDnIIm56FZO15PuY8",
  authDomain: "mychat-df3f0.firebaseapp.com",
  databaseURL: "https://mychat-df3f0.firebaseio.com",
  projectId: "mychat-df3f0",
  storageBucket: "mychat-df3f0.appspot.com",
  messagingSenderId: "785963474083"
};

firebase.initializeApp(firebaseConfig);

export const userService = {
    createUser: (userModel) => {
        return new Promise((resolve, reject) => {
            const email = userModel.email;
            const password = userModel.password;
            firebase
                .auth()
                .createUserWithEmailAndPassword(email, password)
                    .then(user => {
                        resolve(user)
                    }).catch(error => {
                        reject(error)
                    }); 
        });
    }, 
    login: (userModel) => {
        return new Promise((resolve, reject) => {
            const email = userModel.email;
            const password = userModel.password;
            firebase
                .auth()
                .signInWithEmailAndPassword(email, password)
                    .then(user => {
                        resolve(user)
                    }).catch(error => {
                        reject(error)
                    });
        });
    },
    sendMessage: (message) => {
        return new Promise((resolve, reject) => {
            firebase
                .database()
                .ref("messages/")
                .push({
                    message: message,
                    date: new Date().toISOString()
                }, (error) => {
                    if(error) {
                        reject(error);
                    }
                    resolve();
                });
        });
    },
    getMessages: () => {
        return firebase
                .database()
                .ref("messages");
    }
};
